const { MongoClient } = require("mongodb");
const stan = require("node-nats-streaming");

module.exports = function (mongoClient) {
  const conn = stan.connect("test-cluster", "test", {
    url: process.env.BROKER_URL,
  });

  conn.on("connect", () => {
    console.log("Connected to NATS Streaming");

    /* ******************* */
    /* YOUR CODE GOES HERE */
    db = MongoClient.db('mongo')
    const opts = conn.subscriptionOptions().setStartWithLastReceived();
    const subscription = conn.subscribe('users', opts);
    subscription.on('message', (msg) => {
      const event = JSON.parse(msg.getData());
      if(event.eventType === 'UserCreated'){
        const user = event.entityAggregate;
        user._id = event.entityId;
        db.collection('users').insertOne(user);
      }
      else if (event.eventType === 'UserDeleted'){
        const id = event.entityId;
        db.collection('users').deleteOne({ _id: id});
      }
      else{
        console.log("help");
      }
    });
    /* ******************* */
  });

  return conn;
};
