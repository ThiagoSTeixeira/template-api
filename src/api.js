const jwt = require("jsonwebtoken");
const express = require("express");
const cors = require("cors");
const { v4: uuid, v4 } = require("uuid");


//useful functions

const isRequestValid = (body, res) => {
  //verifica se falta algum campo
  for (campo of ['name', 'email', 'password', 'passwordConfirmation']){
      if(!(campo in body)){
          res.status(400);    
          res.send({error: `Request body had missing field {${campo}}`})
          return false;
      }
  }

  //verifica o email
  const rgx = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/
  if(!(body.email.match(rgx))){
      res.status(400);
      res.send({error: `Request body had malformed field {email}`});
      return false;
  }

  //verifica a senha
  if (body.password.length < 8 || body.password.length > 32){
      res.status(400);
      res.send({ error: `Request body had malformed field {password}` });
      return false;
  }

  //confirma a senha
  if (body.password != body.passwordConfirmation){
      res.status(422);
      res.send({error: 'Password confirmation did not match'});
      return false;
  }
  return true;
};

module.exports = function (corsOptions, { stanConn, mongoClient, secret }) {
  const api = express();
  api.use(express.json());
  api.use(cors(corsOptions));

  api.get("/", (req, res) => res.json("Hello, World!"));
  
  /* ******************* */
  /* YOUR CODE GOES HERE */

  //Create
  api.post("/users", async (req, res) => {
    if(isRequestValid(req.body, res)){
      const user = {
        name: req.body.name,
        email: req.body.email,
        password: req.body.password
      }
      id = uuid(); 
      const CEvent = JSON.stringify( {
        eventType: 'UserCreated',
        entityId: id,
        entityAggregate: user
      });
      mongoClient.db("mongo").collection('users').findOne({ id })

      stanConn.publish('users', CEvent);
      res.status(201);
      res.send({user: user});
    }
  });  
  //Delete
  api.delete("/users/:uuid", async (req, res) => {
    if(!req.header("Authentication")){
      res.status(401);
      res.send({error: "Access Token not found"})
      return;
    }
    const auth = req.header("Authentication").split(" ")[1]
    const decToken = jwt.verify(auth, secret);
    if(!(decToken.id === req.params.uuid)){
      res.status(403).json({error: "Access Token did not match User ID"})
      return;
    }
    res.status(200).send({id: req.params.uuid});
    const DEvent = JSON.stringify({
      eventType: "UserDeleted",
      entityId: req.params.uuid,
      entityAggregate: {},
    });
    stanConn.publish('users', DEvent);
  });

  /* ******************* */

  return api;
};
